       implicit real*8 (a-h,o-z)

       COMMON /ALL/ PI,DREF12,DN2,TREF,OM12,BOL,RM12,ZR,TT, 
     & RDOF,EEQ,TSTAR,tetaV(5),ZV(5),IZOT,NVM

       dimension TV(5),TV1(5),BK1(5),BK2(5),BK3(5),BK4(5)

       bol=1.3806d-23        
       PI=3.1415927d0 

       OPEN(1,FILE='input.dan')

        READ(1,*)IZOT
        READ(1,*)RDOF
        READ(1,*)ZR
        READ(1,*)TSTAR
        READ(1,*)DN2 
        READ(1,*)RM1
        READ(1,*)RM2
        READ(1,*)DREF1
        READ(1,*)DREF2
        READ(1,*)ALF1
        READ(1,*)ALF2
        READ(1,*)TREF
        READ(1,*)TT
        READ(1,*)TR
        READ(1,*)TVO
        READ(1,*)TAU
        READ(1,*)TAUMAX
        READ(1,*)NIT
        READ(1,*)ITT
        READ(1,*)NVM
        READ(1,*) (tetaV(j),j=1,NVM)
        READ(1,*)ZV0

    
       CLOSE(1)

       DO i=1,NVM
        TV(I)=TVO
        ZV(I)=ZV0 
       END DO
 

       OM1=0.5+ALF1
       OM2=0.5+ALF2
       RM12=RM1*RM2/(RM1+RM2)
       OM12=(OM1+OM2)/2.
       ALF12=(ALF1+ALF2)/2.
       DREF12=(DREF1+DREF2)/2. 

       VDOF=0.D0
       DO I=1,NVM
        VDOF=VDOF+XIV(I,TVO)
       END DO

       EEQ=(3.*TT+RDOF*TR+VDOF*TVO)
       IF(IZOT.EQ.1.OR.IZOT.EQ.2)EEQ=0.

       FREQ0=SQRT(PI)*
     4 2.*DREF12**2*DN2*(TT/tref)**(1-OM12)* 
     5 SQRT(2.*BOL*TREF/RM12)

C***** ZR0 - Initial value of ZR
CC**** CONSTANT ZR
C       IF(TSTAR.LT.1.D-2)THEN
C        ZR0=ZR
C****  OR ZR (T) DEFINED BY PARKER
C       ELSE 
C        ZR0=ZR/(1.D0+PI**1.5D0*SQRT(TSTAR/TT)/2.D0+
C      1(PI+PI*PI/4.D0)*TSTAR/TT)      
C       END IF
C*********************************
C
C***** ZV0 - Initial value of ZV
CC**** CONSTANT ZV
C       IF(ZV.GT.1.D-4)THEN
C        ZV0=ZV
C****  OR ZV (T) DEFINED BY MW
C       ELSE 
C
C        CONTINUE
C
C       END IF
C*********************************
C


       TIM= 0.D0

       IF(ITT.EQ.0)THEN
        WRITE(*,*)TIM,TVO
       ELSE IF(ITT.EQ.1)THEN
        WRITE(*,*)TIM,TT
       ELSE IF(ITT.EQ.2)THEN
        WRITE(*,*)TIM,TR
	   ELSE IF(ITT.EQ.3)THEN
        WRITE(*,*)TIM,TT,TR,TVO
C       ELSE
C****** valid only if Zr=const
C        WRITE(*,*)TIM*FREQ0/ZR,1.d0 - TR/TEQ
       END IF 

*********** Main LOOP 
*** <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
***********
       DO K=1,NIT
******* ROONGHE-KUTTA, 4-TH ORDER
        TIM=TIM+TAU

        DO I=1,NVM
         TV1(I)=TV(I)
        END DO  
        AK1=FF(1,TR,TV1)*TAU
        DO I=1,NVM
         BK1(I)=FF(I+1,TR,TV1)*TAU
        END DO

        DO I=1,NVM
         TV1(I)=TV(I)+BK1(I)/2.D0
        END DO
        AK2=FF(1,TR+AK1/2.D0,TV1)*TAU  
        DO I=1,NVM
         BK2(I)=FF(I+1,TR+AK1/2.D0,TV1)*TAU  
        END DO

        DO I=1,NVM
         TV1(I)=TV(I)+BK2(I)/2.D0
        END DO
        AK3=FF(1,TR+AK2/2.D0,TV1)*TAU
        DO I=1,NVM 
         BK3(I)=FF(I+1,TR+AK2/2.D0,TV1)*TAU 
        END DO

        DO I=1,NVM
         TV1(I)=TV(I)+BK3(I)
        END DO
        AK4=FF(1,TR+AK3,TV1)*TAU
        DO I=1,NVM 
         BK4(I)=FF(I+1,TR+AK3,TV1)*TAU 
        END DO

        TR=TR+(AK1+2.D0*AK2+2.D0*AK3+AK4)/6.D0
        TV2=TVO
        VDOF=0.d0
        TVO=0.d0
        DO I=1,NVM          
        TV(I)=TV(I)+(BK1(I)+2.D0*BK2(I)+2.D0*BK3(I)+BK4(I))/6.D0
      
C         write(*,*)i,BK1(I),BK2(I),BK3(I),BK4(I)
        
         VDOF=VDOF+XIV(I,TV(I))
         TVO=TVO+XIV(I,TV(I))*TV(I)         
        END DO

        TVO=TVO/VDOF
      
        hug=abs(TVO-TV2)/TVO
        IF(hug.lt.1.d-2)TAU=TAU*1.D-2/HUG
        IF(TAU.GT.TAUMAX)TAU=TAUMAX 
******
       IF(ITT.EQ.0)THEN
        WRITE(*,*)TIM,TVO
       ELSE IF(ITT.EQ.1)THEN
        WRITE(*,*)TIM,TT
       ELSE IF(ITT.EQ.2)THEN
        WRITE(*,*)TIM,TR
	   ELSE IF(ITT.EQ.3)THEN
        WRITE(*,*)TIM,TT,TR,TVO
C       ELSE
C****** valid only if Zr=const
C        WRITE(*,*)TIM*FREQ0/ZR,1.d0 - TR/TEQ
       END IF 


        IF(abs(TVO-TT).lt.1D-0)STOP

       END DO
******* End of Main Loop
******<<<<<<<<<<<<<<<<<<<<<
******************************
6       END

       REAL*8 FUNCTION FF(imode,TR,TV)
       implicit real*8 (a-h,o-z)
       
       COMMON /ALL/ PI,DREF12,DN2,TREF,OM12,BOL,RM12,ZR,TT,
     1 RDOF,EEQ,TSTAR,tetaV(5),ZV(5),IZOT,NVM

       dimension TV(5)


       IF(IZOT.EQ.2)THEN
****** IZOT=2 MEANS THAT TR=TT=CONST
        IF(IMODE.EQ.1)THEN
         FF=TT
         RETURN
        END IF
      
       TR=TT
 
       END IF

       IF(IZOT.EQ.0)THEN 
****** ADIABATIC BATH ****************
        TT=0.D0
        DO J=1,NVM
         TT=TT+XIV(J,TV(J))*TV(J) 
        END DO

        TT=( EEQ - RDOF*TR - TT) /3.D0
       END IF

       FREQ=SQRT(PI)*
     4 2.*DREF12**2*DN2*(TT/tref)**(1-OM12)* 
     5 SQRT(2.*BOL*TREF/RM12)

CCC       COLTIME=1/FREQ

       IF (IMODE.EQ.1)THEN 

C**** CONSTANT ZR
        IF(TSTAR.LT.1.D-2)THEN
        ZRR=ZR
****  OR ZR (T) DEFINED BY PARKER
        ELSE 
         ZRR=ZR/(1.D0+PI**1.5D0*SQRT(TSTAR/TT)/2.D0+
     1   (PI+PI*PI/4.D0)*TSTAR/TT)      
        END IF
*********************************

        FF=(TT-TR)*FREQ/ZRR
        RETURN

       ELSE

C**** CONSTANT ZV
        IF(ZV(IMODE-1).GT.1.D-4)THEN
        ZVV=ZV(IMODE-1)
****  OR ZV (T) DEFINED BY MW
        ELSE 
**************** MILLIKAN / WHITE
         RMA=RM12/0.16603d-26        
         AA=1.16D-3*dsqrt(RMA)*tetaV(IMODE-1)**(4D0/3D0)
         BB=(-1.74D-5)*RMA**0.75D0*tetaV(IMODE-1)**(4D0/3D0)-18.42D0        
         PTLT=101325D0*EXP(AA*TT**(-1D0/3D0)+BB)
         TAULT=PTLT/(BOL*TT*DN2) 
**************** PARK
         CSR=(2D0/SQRT(PI))*SQRT(BOL*TT/RM12)
         TAUP=1D0/(DN2*1D-20*CSR)

         ZVV=FREQ*(TAULT+TAUP)         

        END IF
*********************************

C        WRITE(*,*)XIV(TT),XIV(TV),FF

         FF=(TT*XIV(IMODE-1,TT)-TV(IMODE-1)*XIV(IMODE-1,TV(IMODE-1)))
         FF=FF*FREQ/ZVV

C         WRITE(*,*)TT,TV(IMODE-1),FF,XIV(IMODE-1,TV(IMODE-1)),ZVV
C         READ(*,*)

         FF=FF*2d0/(XIV(IMODE-1,TV(IMODE-1))**2D0*
     1   DEXP(TETAV(IMODE-1)/TV(IMODE-1)))

C        WRITE(*,*)XIV(TT),XIV(TV),TT,TV
C        READ(*,*)

        RETURN

       END IF

       END


       REAL*8 FUNCTION XIV(J,TV)
       implicit real*8 (a-h,o-z)

       COMMON /ALL/ PI,DREF12,DN2,TREF,OM12,BOL,RM12,ZR,TT,
     1 RDOF,EEQ,TSTAR,tetaV(5),ZV(5),IZOT,NVM


       IF(TETAV(J).LT.1.d-2)THEN
        XIV=0.
        RETURN
       END IF

       IF(TV/tetaV(J).GT.1.d-2)THEN


        XIV=2.d0*(TETAV(J)/TV)/(DEXP(TETAV(J)/TV)-1.D0)
       ELSE
        XIV=1.d-2
       END IF   

       RETURN
       END
